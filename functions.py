#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 16:19:31 2020

@author: Matuszynska
"""
import numpy as np
from modelbase.ode import Simulator

   
def changingLight(model, y0d, PFD=125., dim =0.):
    r"""Performs PAM light-dim light simulation for given light intensity.

    The length of the experiment is fixed to the length of accompanying lab experiments 
    
    Parameters
    ----------
    model : modelbase object
    y0d : numpy.array or list
        initial concentrations of compounds
    PFD : int
        light intensity
    dark : int 
        intensity of teh dim phase set to 0 as default
    

    Returns
    -------
    s : modelbase object
        contains results of the simulation
     
    """
    tprot = np.array([1.,   0.8] + [2., 0.8] + [5,   0.8]*2 + 
                  [15.86666667,   0.8] + [30.86666667,   0.8 ]*11 + 
                  [15.53333333,   0.8] + [30.86666667,   0.8 ]*15)
    
    ProtPFDs = np.array([0., 5000.]*2 + [PFD, 5000.] * 14 + [dim, 5000.] * 16 )
    
    
    s = Simulator(model)
    s.initialise(y0d)
    dt = 0
    for i in range(len(tprot)):
        s.update_parameter('pfd', ProtPFDs[i])
        dt += tprot[i]
        s.simulate(dt, **{"rtol":1e-14,"atol":1e-10, "maxnef": 20, "maxncf":10})
    return s