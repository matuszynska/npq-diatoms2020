This project supports the study published as **Impaired photoprotection in Phaeodactylum tricornutum KEA3 mutants reveals the proton regulatory circuit of diatoms light acclimation** by Seydoux et al [*] and contain a computational model of non-photochemical quenching (NPQ) in diatoms.

# Installation
I recommend creating conda [environment.yml](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#create-env-file-manually) for this project using the environment.yml file that includes dependencies:

    name: py38
    channels:
      - conda-forge
    dependencies:
      - jupyter
      - python=3.8
      - scipy=1.7.2
      - modelbase=1.38
    
Then create and activate the conda environment using commands below:

    conda env create --file environment.yml
    conda activate py37

Alternatively, you can use pip installation, but then you will have to install Assimulo ( package for solving ordinary differential equations) manually:

```pip install -r requirements.txt```

Run notebooks in the analysis folder via jupyter notebook

```jupyter notebook```.

# Content
To reproduce all figures published in the manuscript [*] simply run  [manuscript_figures.ipynb](https://gitlab.com/matuszynska/npq-diatoms2020/-/blob/master/manuscript_figures.ipynb).
Other files contain:
- [model.py](https://gitlab.com/matuszynska/npq-diatoms2020/-/blob/master/model.py) contains the Python implementation of the mathematical model of NPQ in diatoms,
- [functions.py](https://gitlab.com/matuszynska/npq-diatoms2020/-/blob/master/functions.py) contains function changingLight that reproduces the experimental protocol used in the wet lab,
- [get_NPQ.py](https://gitlab.com/matuszynska/npq-diatoms2020/-/blob/master/get_NPQ.py) contains function calculating NPQ from the caluclated fluorescence signal, using function:

- [run_pam.py](https://gitlab.com/matuszynska/npq-diatoms2020/-/blob/master/run_pam.py) contains function that performs PAM simulation on the model created in model.py. It uses the light function from functions.py and calls the get_NPQ function to calculate the NPQ. It produces Fig. 3 from the manuscript [*].

# Model
Simplified model of quenching in diatoms, based on the ETC model for Arabidopsis, as described in Matuszynska et al. 2016 [1] implemented in Python usin modelbase software [2].

[1] Matuszyńska, A., Heidari, S., Jahns, P., Ebenhöh, O., 2016. A mathematical model of non-photochemical quenching to study short-term light memory in plants. Biochimica et Biophysica Acta (BBA) - Bioenergetics 1857, 1860–1869. https://doi.org/10.1016/j.bbabio.2016.09.003

[2] van Aalst M, Ebenhöh O, Matuszyńska A. 2021. Constructing and analysing dynamic models with modelbase v1.2.3: a software update. BMC Bioinformatics. 20;22(1):203. [doi: 10.1186/s12859-021-04122-7](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-021-04122-7). 

![scheme](scheme.png "Scheme of the model")
Scheme of the compounds and reactions of the photosynthetic electron transport chain included in the model. The highly simplified computational model of non-photochemical quenching (NPQ) in diatoms.

## Mathematical description
The mathematical model comprises a set of five ordinary differential equations (ODEs) following the dynamics of the:
- reduced state of the plastoquinones (PQ), 
- the stromal concentration of ATP, 
- the luminal proton concentration (H), 
- the fraction of epoxidised xanthophylls (DD), and 
- the fraction of active ATPase.
The model calculates also the fluorescence signal emitted from PSII.

## Main assumption
To test the hypothesis that quencher is linearly dependent only on the deepoxidation state of the xanthophyll cycle we abandoned the four-state quenching model introduced in [1]. Instead, quencher (Q) is calculated as

```math
Q = [Dt] * [LHCSR]
```

where [Dt] is a dynamic variable representing the concentration of diatoxanthin and [LHCSR] is not a dynamic variable anymore, but a parameter corresponding to the amount of available LHCSR protein, set to 0.5.

K+ efflux antiporter 3 (KEA3) transporter that exchanges K+/H+ ions between the membrane is affecting the NPQ dynamics.

## Changes
We made several important changes to account for physiological differences between diatoms and plants, changing six parameters: 
- the mid point potential of the cytb6f decreased from 0.380 to 0.350,
- Fv/Fm of a diatom is lower than of the plant, ranging between 0.6-0.7. Hence the charge separation rate constants needed to differ. To obtain the Fv/Fm=0.68 we lowered the rate of charge separation (kP used to be 5e9, now 1.2e9)

```
fvfm = M16model.get_parameter('kP')/(M16model.get_parameter('kF') + M16model.get_parameter('kP') + M16model.get_parameter('kH0') + M16model.get_parameter('kH')*Q)
```

- lumen and stroma volumes are in diatoms similar. Therefore lumen is much bigger than we assumed before and the accumulated protons will have slower effect than before. I have changed the fuction calculating the lumenal pH from the H concentration to account for the bigger lumen. Ideally, I should have also follow now dynamically the concentration of stromal H, but I have obtained already good qualitative agreement,
- rate constants of the deepoxidation and epoxidation are of the similar range.

Additionally, to test the hypothesis regarding the role of KEA3 we:
- introduced K+ efflux antiporter 3 (KEA3) transporter exchanges K+/H+ ions between the membrane, keeping the pmf intact, but affecting the ∆pH,
- introduced a new parameter kETC scaling NPQ activity under low light intensities, to account for the recently discovered high light-dependency of the rate constant of the epoxidase enzyme.

# How to cite this material
10.5281/zenodo.5865038
