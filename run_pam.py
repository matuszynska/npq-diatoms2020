#!/usr/bin/env python
# coding: utf-8

# ### repeated analysis for the manuscript

# In[1]:


get_ipython().run_line_magic('matplotlib', 'inline')


# Few reminders regaring impact of some numbers
# 
# decrease od the parameter KphSatZcauses re-occurance of the transient NPQ 

# In[2]:


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from modelbase.ode import Simulator

from model import model_diatom
import functions
import get_NPQ


# In[5]:


colpal = {'Mutant 1': 'limegreen',
          'Mutant 2': 'darkgreen',
          'WT': 'red',
          'Overexpressor 1': 'blue',
}
leak_var = {'Mutant 1': 0.865, # 0.9
            'WT': .7, # WT corresponding to kKEA = 0.5
            'Overexpressor 1': 0.65,
           }


y0 =  {"P": 5., "H": 5.56305280e-04, "E": 0.5, "A": 25., "Dd": .9}

fig1, axNPQ = plt.subplots()
fig3, axdt2 = plt.subplots()


data = dict()
  


for leak in leak_var.keys():

    model_diatom.update_parameter('kkea', leak_var[leak])


    s = Simulator(model_diatom)
    s.initialise(y0)

    # dark adaptation: making sure that the initial conditions are representing
    # healthy state of the diatom with oxidised PQ pool and no active quencher
    s.update_parameters({'pfd': 0.,
                         'kkea': leak_var[leak]})
    s.simulate(1000, steps=3000, **{"atol":1e-8})
    y0d = s.get_results_array()[-1]


    PAM1 = functions.changingLight(model_diatom, y0d, PFD=200., dim =0.)
    F = PAM1.get_variable('Fluo')
    Fm, NPQ, tm, Fo, to = get_NPQ.get_NPQ(PAM1.get_variable('Fluo'), PAM1.get_time(), PAM1.get_variable('L'), 5000)

    axNPQ.plot(tm/60., NPQ,'s-', label=str(round(leak_var[leak]/(1-leak_var[leak]),2)), color=colpal[leak])
    axNPQ.set_xlabel('Time [s]')
    axNPQ.set_ylabel('NPQ [normalized]')       

    axdt2.scatter(NPQ, [PAM1.get_variable('Dt')[i]*100 for i in [list(PAM1.get_time()).index(x) for x in tm]], marker='s', 
                        label=str(leak_var[leak]), color=colpal[leak])
    axdt2.set_xlabel('NPQ')
    axdt2.set_ylabel('DES %')

    data[leak] = {'time': tm, 'NPQ': NPQ}

    # export raw data as a csv file
    pd.DataFrame(data[leak]).to_csv("file"+str(leak)+".csv")

axNPQ.legend(title='$k_{KEA3}$')
axdt2.legend()

plt.show()



